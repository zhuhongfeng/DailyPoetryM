﻿namespace DailyPoetryM;
//将所有页面名写成字符串,方便导航,防止字符串硬编码
public static class Constants
{
    public const string MainPage = nameof(MainPage);
    public const string QueryPage=nameof(QueryPage);
    public const string FavoritePage = nameof(FavoritePage);
    public const string DetailPage=nameof(DetailPage);
    public const string TodayPage = nameof(TodayPage);
    public const string TodayDetailPage = nameof(TodayDetailPage);
}