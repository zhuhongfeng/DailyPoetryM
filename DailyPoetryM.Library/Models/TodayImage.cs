﻿namespace DailyPoetryM.Models;
/// <summary>
/// 今日图片信息类
/// </summary>
public class TodayImage
{
    public string FullStartDate { get; set; }=string.Empty;
    public DateTime ExpiresAt { get; set; }//过期时间
    public string Copyright { get; set; } = string.Empty;
    public string CopyrightLink { get; set; }= string.Empty;
    public byte[]? ImageBytes { get; set; }//图片的byte数组，尽量使用基本类型，已下载的图片
}