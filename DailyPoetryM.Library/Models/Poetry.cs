﻿namespace DailyPoetryM.Models;

[Table("works")]//映射sqlite的表名
public class Poetry
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = string.Empty;

    [Column("author_name")]
    public string Author { get; set; } = string.Empty;

    [Column("dynasty")]
    public string Dynasty { get; set; } = string.Empty;

    [Column("content")]
    public string Content { get; set; } = string.Empty;

    //搜索时，显示诗词内容的第一句话
    private string? _snippet;
    [Ignore]//不映射到数据库
    public string Snippet =>
        _snippet ??= Content.Split('。')[0].Replace("\r\n", " ");
}