﻿namespace DailyPoetryM.Models;
//收藏在另一个数据库中，诗词在另一个数据库中
public partial class Favorite:ObservableObject
{
    [PrimaryKey]
    public int PoetyrId { get; set; }
    [ObservableProperty]
    private bool isFavorite;
    public long Timestamp { get; set; }//时间戳，用于将最后收藏的额内容显示在上面
}