﻿namespace DailyPoetryM.Models;
//今日诗词API返回的数据
public class TodayPoetry
{
    public string Snippet { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public string Dynasty { get; set; } = string.Empty;
    public string Author { get; set; } = string.Empty;
    public string Content { get; set; } = string.Empty;
    //诗词数据的来源，如果断网了就从本地随机取
    public string Source { get; set; } = string.Empty;
}