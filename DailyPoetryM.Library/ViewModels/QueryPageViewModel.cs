﻿namespace DailyPoetryM.ViewModels;
//Model可以提供数据，ViewModel可以提供功能
//外层ViewModel
public partial class QueryPageViewModel : BaseViewModel
{
    private readonly INavigationService _navigationService;

    public QueryPageViewModel(INavigationService navigationService)
    {
        _navigationService = navigationService;
        //默认已经有一个，添加搜索条件时直接new，不要使用依赖注入
        FilterViewModels.Add(new FilterViewModel(this));
    }
    public ObservableCollection<FilterViewModel> FilterViewModels { get; } = new();


    [RelayCommand]
    private async Task Query()
    {
        //让C#生成一段可执行的C#代码，元编程，meta-programming
        //把用户选择的条件变成一个查询，比如x.Name.Contains("XX")
        var parameter = Expression.Parameter(typeof(Poetry), "x");

        //Where过滤掉没填条件的ViewModel
        //然后使用Select将ViewModel列表转换成Expression列表
        //Aggregate将多个Expression组合在一起
        var aggregatedExpression = FilterViewModels.Where(x => !string.IsNullOrWhiteSpace(x.Content))
              .Select(x => GetExpression(parameter, x))
              .Aggregate(Expression.Constant(true) as Expression, Expression.AndAlso);
        //完整的Lambda表达式条件
        var where = Expression.Lambda<Func<Poetry, bool>>(aggregatedExpression, parameter);
        //导航到MainPage，然后将查询条件作为参数传递过去子
        await _navigationService.NavigateToAsync($"//{Constants.MainPage}", new Dictionary<string, object>{{nameof(where),where}});

    }
    //将一个FilterViewModel翻译成表达式条件x.[Property].Contains("[condition]")
    private static Expression GetExpression(ParameterExpression parameter, FilterViewModel filterViewModel)
    {
        //x.[Property],根据filterViewModel.Type.PropertyName读取parameter的属性
        var property = Expression.Property(parameter, filterViewModel.Type.PropertyName);
        //.Contains()，使用反射获取Contains方法，是string.Contains(string str)
        var method = typeof(string).GetMethod(nameof(string.Contains), new[] { typeof(string) });
        //"[condition]"，是Contains方法的参数
        var condition = Expression.Constant(filterViewModel.Content, typeof(string));
        //拼接表达式
        return Expression.Call(property, method!, condition);
    }


    public void AddFilter(FilterViewModel filterViewModel)
    {
        //在当前条件后面添加一个
        FilterViewModels.Insert(FilterViewModels.IndexOf(filterViewModel)+1, new FilterViewModel(this));
    }
    public void RemoveFilter(FilterViewModel filterViewModel)
    {
        //删除最后一个要检查下，必须保留一个
        FilterViewModels.Remove(filterViewModel);
        if (FilterViewModels.Count == 0)
        {
            FilterViewModels.Add(new FilterViewModel(this));
        }
    }
}
//内层ViewModel，组成CollectionView中的单个item
public partial class FilterViewModel : ObservableObject
{
    //持有外层VeiwModel的引用
    private readonly QueryPageViewModel _queryPageViewModel;
    public FilterViewModel(QueryPageViewModel queryPageViewModel)
    {
        _queryPageViewModel = queryPageViewModel;
    }
    [ObservableProperty]
    private string content;
    [ObservableProperty]
    private FilterType type = FilterType.NameFilter;

    //内层ViewModel需要让外层ViewModel执行动作，因此需要持有外层VeiwModel的引用
    [RelayCommand]
    private void Add() => _queryPageViewModel.AddFilter(this);

    [RelayCommand]
    private void Remove() => _queryPageViewModel.RemoveFilter(this);
}

public class FilterType
{
    public static readonly FilterType NameFilter = new("标题", nameof(Poetry.Name));
    public static readonly FilterType AuthorFilter = new("作者", nameof(Poetry.Author));
    public static readonly FilterType ContentFilter = new("内容", nameof(Poetry.Content));
    //实例数量已知，类似于枚举
    public static List<FilterType> FilterTypes { get; } = new()
    {
        NameFilter,AuthorFilter,ContentFilter
    };
    //private不想让其他类使用这个类，只能自己创建，这样就能控制类只能有多少个实例
    private FilterType(string name, string propertyName)
    {
        Name = name;
        PropertyName= propertyName;
    }
    public string Name { get; }//Picker中显示的名字
    public string PropertyName { get; }//查询Poetry的字段名

}