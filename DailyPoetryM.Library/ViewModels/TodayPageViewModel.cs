﻿namespace DailyPoetryM.ViewModels;

public partial class TodayPageViewModel:BaseViewModel
{
    private readonly ITodayPoetryService _todayPoetryService;
    private readonly INavigationService _navigationService;
    private readonly IBingImageService _bingImageService;
    private readonly IBrowserService _browserService;

    public TodayPageViewModel(ITodayPoetryService todayPoetryService,INavigationService navigationService,IBingImageService bingImageService,IBrowserService browserService)
    {
        _todayPoetryService = todayPoetryService;
        _navigationService = navigationService;
        _bingImageService = bingImageService;
        _browserService = browserService;
    }
   
    [ObservableProperty]
    private TodayPoetry todayPoetry;
    [ObservableProperty] 
    private TodayImage todayImage; 

    [RelayCommand]
    private void LoadPoetry()
    {
        //人为开启两个线程，同步执行以下两个操作，因为他们之间不会相互影响
        //如果一个结果会影响下一个结果，必须使用async/await去等待后台线程执行
        //await时，主线程会将操作然给后台的10个线程池中的某个去执行，而不是卡主线程
        //并记住当前的执行状态，等到后台返回结果时，之线程会回忆状态继续执行后续代码
        Task.Run(async () =>
        {
            //先显示图片，再去检查更新，如果有更新再更新图片
            TodayImage = await _bingImageService.GetTodayImageAsync();
            var checkUpdateResult = await _bingImageService.CheckUpdateAsync();
            if (checkUpdateResult.HasUpdate)
            {
                TodayImage = checkUpdateResult.TodayImage;
            }
        });
        Task.Run(async () =>
        {
            if (IsLoading) return;
            IsLoading = true;
            TodayPoetry = await _todayPoetryService.GetTodayPoetryAsync();
            IsLoading=false;
        });
        
    }

    [RelayCommand]
    private async Task NavigateToDetailAsync()
    {
        await _navigationService.NavigateToAsync(Constants.TodayDetailPage,
            new Dictionary<string, object>
            {
                {nameof(TodayPoetry), TodayPoetry}
            });
    }

    [RelayCommand]
    private  Task JinrishiciAsync()=> _browserService.OpenAsync("https://www.jinrishici.com");

    [RelayCommand]
    private Task CopyrightAsync() => _browserService.OpenAsync(TodayImage.CopyrightLink);
}