﻿namespace DailyPoetryM.ViewModels;

[QueryProperty(nameof(TodayPoetry),nameof(TodayPoetry))]
public partial class TodayDetailPageViewModel:BaseViewModel
{
    [ObservableProperty]
    private TodayPoetry todayPoetry;
}