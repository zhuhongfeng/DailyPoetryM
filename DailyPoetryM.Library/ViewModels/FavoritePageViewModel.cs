﻿namespace DailyPoetryM.ViewModels;

public partial class FavoritePageViewModel : BaseViewModel
{
    private readonly IPoetryService _poetryService;
    private readonly IFavoriteService _favoriteService;
    private readonly INavigationService _navigationService;

    public FavoritePageViewModel(IPoetryService poetryService, IFavoriteService favoriteService, INavigationService navigationService)
    {
        _poetryService = poetryService;
        _favoriteService = favoriteService;
        _navigationService = navigationService;
        //4、响应收藏更新事件，处理事件的参数。
        favoriteService.Updated+=FavoriteServiceOnUpdated;
    }

    private async void FavoriteServiceOnUpdated(object? sender, FavoriteUpdatedEventArgs e)
    {
        var favorite = e.UpdatedFavorite;
        if (favorite.IsFavorite)
        {
            //收藏，增加收藏列表
            var poetryFavorite = new PoetryFavorite()
            {
                Poetry = await _poetryService.GetPoetryAsync(favorite.PoetyrId),
                Favorite = favorite
            };
            //按照时间戳，判断插入诗词收藏的位置
            var index = PoetryFavorites.IndexOf(
                PoetryFavorites.FirstOrDefault(x => x.Favorite.Timestamp < favorite.Timestamp)!);
            if (index < 0)
            {
                index=PoetryFavorites.Count;
            }
            PoetryFavorites.Insert(index,poetryFavorite);
        }
        else
        {
            //取消收藏，移移除收藏列表
            PoetryFavorites.Remove(PoetryFavorites.FirstOrDefault(x => x.Favorite.PoetyrId == favorite.PoetyrId)!);
        }
    }

    public ObservableCollection<PoetryFavorite> PoetryFavorites { get; } = new();

    [RelayCommand]
    private async Task LoadedAsync()
    {
        if (IsLoading) return;
        IsLoading=true;
        PoetryFavorites.Clear();

        //因为有2个数据库，因此需要从收藏数据库查询收藏数据
        //然后从诗词数据库查询诗词内容
        var favoriteList = await _favoriteService.GetFavoritesAsync();
        //foreach (var favorite in favoriteList)
        //{
        //    var poetry = await _poetryService.GetPoetryAsync(favorite.PoetyrId);
        //    var poetryFavorite = new PoetryFavorite()
        //    {
        //        Poetry = poetry,
        //        Favorite = favorite
        //    };
        //    PoetryFavorites.Add(poetryFavorite);
        //}

        //查询并组合二者信息，下面的做法替代foreach操作，Linq，简洁高效
        var poetryFavorites = await Task.WhenAll(favoriteList.Select(x =>
             Task.Run(async () => new PoetryFavorite
             {
                 Poetry = await _poetryService.GetPoetryAsync(x.PoetyrId),
                 Favorite = x
             })));
        foreach (var poetryFavorite in poetryFavorites)
        {
            PoetryFavorites.Add(poetryFavorite);
        }

        IsLoading=false;
    }

    [RelayCommand]
    private Task NavigateToDetailAsync(PoetryFavorite poetryFavorite) => _navigationService.NavigateToAsync(Constants.DetailPage,
        new Dictionary<string, object>
        {
            {nameof(Poetry),poetryFavorite.Poetry}
        });
}

public class PoetryFavorite
{
    public Poetry Poetry { get; set; }
    public Favorite Favorite { get; set; }
}