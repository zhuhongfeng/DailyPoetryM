﻿namespace DailyPoetryM.ViewModels;

//用于显示搜索结果

//[QueryProperty(nameof(Where), nameof(where))] //参数比较复杂，传参是会报错误
public partial class ResultPageViewModel : BaseViewModel,IQueryAttributable
{
    //显式实现IQueryAttributable传参，使用强制类型转换
    public void ApplyQueryAttributes(IDictionary<string, object> query)
    {
        //如果没有传递参数，那么query时空的，则Where需要一个默认值
        Where=(Expression<Func<Poetry, bool>>)query[nameof(where)];
    }

    public const int PageSize = 20;
    private readonly IPoetryService _poetryService;
    private readonly INavigationService _navigationService;
    private readonly IFavoriteService _favoriteService;

    public ResultPageViewModel(IPoetryService poetryService, INavigationService navigationService,IFavoriteService favoriteService)
    {
        _poetryService = poetryService;
        _navigationService = navigationService;
        _favoriteService = favoriteService;
    }

    #region 属性
    public ObservableCollection<Poetry> Poetries { get; } = new();

    [ObservableProperty]
    private Expression<Func<Poetry, bool>>? where;

    #endregion

    #region Command
    [RelayCommand]
    private async Task NavigatedToAsync()
    {
        //todo:在此初始化项目的两个数据库，因为没有找到一个好的初始化方案
        if (!_poetryService.IsInitialized) await _poetryService.InitializeAsync();
        if (!_favoriteService.IsInitialized) await _favoriteService.InitializeAsync();

        //如果没有传递参数，那么query是空的，则Where需要一个默认值
        Where ??= Expression.Lambda<Func<Poetry, bool>>(Expression.Constant(true), Expression.Parameter(typeof(Poetry), "x"));

        await GetPoetries();
    }

    [RelayCommand]
    private async Task RefreshAsync()
    {
        //刷新时强行初始化查询条件
        Where = Expression.Lambda<Func<Poetry, bool>>(Expression.Constant(true), Expression.Parameter(typeof(Poetry), "x"));

        await GetPoetries();
    }

    private async Task GetPoetries()
    {
        if (IsBusy) return;
        try
        {
            IsBusy = true;
            Poetries.Clear();
            var poetryList = await _poetryService.GetPoetryListAsync(Where, Poetries.Count, PageSize);
            foreach (var poetry in poetryList)
            {
                Poetries.Add(poetry);
            }
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            await Shell.Current.DisplayAlert("Error!", $"Unable to get Poetries:{ex.Message}", "OK");
        }
        finally
        {
            IsBusy=false;
            IsRefreshing=false;
        }
    }

    [RelayCommand]
    private async Task LoadMoreDataAsync()
    {
        if (IsBusy) return;//如果正在导航，就别再加载了
        if (IsLoading) return;
        try
        {
            IsLoading=true;
            var poetryList = await _poetryService.GetPoetryListAsync(Where, Poetries.Count, PageSize);
            foreach (var poetry in poetryList)
            {
                Poetries.Add(poetry);
            }
        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex);
            await Shell.Current.DisplayAlert("Error!", $"Unable to get Poetries:{ex.Message}", "OK");
        }
        finally
        {
            IsLoading=false;
            IsRefreshing=false;
        }
    }

    [RelayCommand]
    private Task NavigateToDetailAsync(Poetry poetry) => _navigationService.NavigateToAsync(Constants.DetailPage,
            new Dictionary<string, object>
            {
                {nameof(Poetry),poetry}
            });

    #endregion

    
}