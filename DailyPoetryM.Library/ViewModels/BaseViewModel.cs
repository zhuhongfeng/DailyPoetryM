﻿namespace DailyPoetryM.ViewModels;

public partial class BaseViewModel:ObservableObject
{
    public const int PageSize = 20;
    [ObservableProperty]
    private bool isBusy;
    [ObservableProperty]
    private bool isLoading;
    [ObservableProperty]
    private bool isRefreshing;

    [ObservableProperty]
    private string title;
}