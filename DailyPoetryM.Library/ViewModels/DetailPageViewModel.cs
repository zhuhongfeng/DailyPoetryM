﻿namespace DailyPoetryM.ViewModels;

[QueryProperty(nameof(Poetry), nameof(Poetry))]
public partial class DetailPageViewModel : BaseViewModel
{
    private bool _isFavorite;//保留从数据库读取诗词原始的收藏状态
    private readonly IFavoriteService _favoriteService;
    public DetailPageViewModel(IFavoriteService favoriteService)
    {
        _favoriteService = favoriteService;
        IsBusy = true;
    }

    [ObservableProperty]
    private Poetry poetry;

    [ObservableProperty]
    private Favorite? favorite;
    //导航到当前页面之后，此时参数已经设置好了，因此可以开始读取收藏信息
    [RelayCommand]
    private async Task NavigatedToAsync()
    {
        //如果收藏数据库中没有数据，那么需要新建一个诗词收藏对象
        Favorite=await _favoriteService.GetFavoriteAsync(Poetry.Id) ??
                 new Favorite { PoetyrId = Poetry.Id, IsFavorite = false };
        _isFavorite = Favorite.IsFavorite;
        IsBusy = false;
    }

    [RelayCommand]
    private async Task FavoriteToggledAsync()
    {
        //如果诗词已经收藏，页面加载时会先触发此事件，因此在构造函数中设置busy，跳过事件
        if(IsBusy)return;
        //如果当前收藏状态和原始收藏状态一致，则不要执行保存
        if (_isFavorite == Favorite.IsFavorite) return;
        
        await _favoriteService.SaveFavoriteAsync(Favorite);
    }
}