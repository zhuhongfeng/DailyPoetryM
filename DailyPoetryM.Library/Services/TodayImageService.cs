﻿namespace DailyPoetryM.Services;

public interface ITodayImageService
{
    Task<TodayImage> GetTodayImageAsync(bool includingImageStream);
    //保存图片，或者只更新过期时间
    Task SaveTodayImageAsync(TodayImage todayImage,bool savingExpiresAtOnly);
    //从服务器下载数据

}
public class TodayImageService:ITodayImageService
{
    public const string FullStartDateKey = $"{nameof(TodayImageService)}.{nameof(TodayImage.FullStartDate)}";
    public const string ExpiresAtKey = $"{nameof(TodayImageService)}.{nameof(TodayImage.ExpiresAt)}";
    public const string CopyrightKey = $"{nameof(TodayImageService)}.{nameof(TodayImage.Copyright)}";
    public const string CopyrightLinkKey = $"{nameof(TodayImageService)}.{nameof(TodayImage.CopyrightLink)}";
    public const string FullStartDateDefault = "202401051600";
    public const string CopyrightDefault = "莫生气 (© 2007-2023 moyublog.com)";
    public const string CopyrightLinkDefault =
        "https://www.moyublog.com/desk/21060.html";

    public const string ImageName = "moshengqi.bin";
    public const string TodayImageName = "todayimage.bin";
    public static readonly string TodayImagePath =
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), TodayImageName);

    private readonly IPreferencesService _preferencesService;
    public TodayImageService(IPreferencesService preferencesService)
    {
        _preferencesService = preferencesService;
    }


    public async Task<TodayImage> GetTodayImageAsync(bool includingImageStream)
    {
        var todayImage = new TodayImage
        {
            FullStartDate = _preferencesService.Get(FullStartDateKey,FullStartDateDefault),//必须赋一个初始值
            ExpiresAt = _preferencesService.Get(ExpiresAtKey,DateTime.Now),
            Copyright = _preferencesService.Get(CopyrightKey,CopyrightDefault),
            CopyrightLink = _preferencesService.Get(CopyrightLinkKey,CopyrightLinkDefault)
        };
        //当今日图片缓存不存在的情况下，将嵌入的资源图片显示出来
        if (!File.Exists(TodayImagePath))
        {
            await using var imageAssetFileStream = new FileStream(TodayImagePath, FileMode.Create)?? throw new NullReferenceException("Null file stream");
            var assetName = $"{typeof(TodayImageService).Namespace.Split('.')[0]}.{ImageName}";
            await using var imageAssetStream = typeof(TodayImageService).Assembly.GetManifestResourceStream(assetName)??throw new Exception($"找不到名为{assetName}的资源");

            await imageAssetStream.CopyToAsync(imageAssetFileStream);
        }

        if (!includingImageStream) return todayImage;//如果不想要图片只返回信息即可
        await using var imageMemoryStream = new MemoryStream();
        await using var imageFileStream = new FileStream(TodayImagePath, FileMode.Open);
        await imageFileStream.CopyToAsync(imageMemoryStream);
        todayImage.ImageBytes = imageMemoryStream.ToArray();//只有内存流才能转成byte数组

        return todayImage;
    }

    public async Task SaveTodayImageAsync(TodayImage todayImage, bool savingExpiresAtOnly)
    {
        _preferencesService.Set(ExpiresAtKey,todayImage.ExpiresAt);
        if (savingExpiresAtOnly) return;//只更新过期时间
        if (todayImage.ImageBytes == null)
        {
            throw new ArgumentException("null image bytes.", nameof(todayImage));
        }
        _preferencesService.Set(FullStartDateKey,todayImage.FullStartDate);
        _preferencesService.Set(CopyrightKey,todayImage.Copyright);
        _preferencesService.Set(CopyrightLinkKey,todayImage.CopyrightLink);
        //保存图片
        await using var imageFileStream = new FileStream(TodayImagePath, FileMode.Create);
        await imageFileStream.WriteAsync(todayImage.ImageBytes,0,todayImage.ImageBytes.Length);
    }
}