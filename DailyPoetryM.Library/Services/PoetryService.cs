﻿namespace DailyPoetryM.Services;
public interface IPoetryService
{
    bool IsInitialized { get; }//判断数据库是否初始化
    /// <summary>
    /// 初始化
    /// </summary>
    Task InitializeAsync();
    /// <summary>
    /// 返回一首诗
    /// </summary>
    Task<Poetry> GetPoetryAsync(int id);
    /// <summary>
    /// 返回诗词列表，参数是一个表达式树，where语句，skip和take用于翻页功能
    /// </summary>
    Task<IEnumerable<Poetry>> GetPoetryListAsync(Expression<Func<Poetry, bool>> where, int skip, int take);
}
//用来存储这个类中使用的常量
public static class PoetryServiceConst
{
    public const int Version = 1;//代表正确的版本号，每次发布版本时需要改这个版本号
    public const string VersionKey = $"{nameof(PoetryServiceConst)}.{nameof(Version)}";
    //代表程序偏好存储（键值对）的版本
}
public class PoetryService : IPoetryService
{
    private readonly IPreferencesService _preferencesService;
    public PoetryService(IPreferencesService preferencesService)
    {
        _preferencesService = preferencesService;
    }
    //读取本地存储的键值对，如果两个版本不一样，则需要执行初始化
    public bool IsInitialized => _preferencesService.Get(PoetryServiceConst.VersionKey, 0)== PoetryServiceConst.Version;

    //准备好文件名和文件地址（目标机器的本地文件）
    public const string DbName = "poetrydb.sqlite3";
    public static readonly string PoetryDbPath =
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), DbName);
    //数据库连接
    private SQLiteAsyncConnection? _connection;
    private SQLiteAsyncConnection Connection => _connection ??= new SQLiteAsyncConnection(PoetryDbPath);


    public async Task InitializeAsync()
    {
        //如何判断数据库是否被初始化?
        //比较存储数据库的版本号（本地[老的版本号]和服务器[新的版本号]）
        //部署操作：把嵌入的资源读出来，打开目标文件，再写到文件中（将资源内容拷贝到打开的目标文件）。

        //使用using 保证文件操作完，自动关闭
        await using var dbFileStream = new FileStream(PoetryDbPath, FileMode.OpenOrCreate);
        //打开资源文件,.Assembly是项目编译后的程序集，获取资源流
        var assetName = $"{typeof(PoetryService).Namespace.Split('.')[0]}.{DbName}";
        await using var dbAssetStream = typeof(PoetryService).Assembly.GetManifestResourceStream(assetName)??throw new Exception($"找不到名为{assetName}的资源");
        /*//防御性编程，主动抛出异常
        if (dbAssetStream is null)
        {
            throw new Exception($"找不到名为{assetName}的资源");
        }*/
        //拷贝文件
        await dbAssetStream.CopyToAsync(dbFileStream);
        //存版本号
        _preferencesService.Set(PoetryServiceConst.VersionKey, PoetryServiceConst.Version);
    }

    public async Task<Poetry> GetPoetryAsync(int id) =>
        await Connection.Table<Poetry>().FirstOrDefaultAsync(x => x.Id == id);

    public async Task<IEnumerable<Poetry>> GetPoetryListAsync(Expression<Func<Poetry, bool>> where, int skip, int take) =>
        await Connection.Table<Poetry>().Where(where).Skip(skip).Take(take).ToListAsync();

    public async Task CloseAsync() => await Connection.CloseAsync();
}