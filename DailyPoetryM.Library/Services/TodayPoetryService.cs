﻿namespace DailyPoetryM.Services;
public static class TodayPoetrySource
{
    public const string Jinrishici = nameof(Jinrishici);//代表今日诗词API
    public const string Local = nameof(Local);//本地
}
public interface ITodayPoetryService
{
    Task<TodayPoetry> GetTodayPoetryAsync();
}
public class TodayPoetryService : ITodayPoetryService
{
    public const string TokenKey = $"{nameof(TodayPoetryService)}.Token";
    private readonly IAlertService _alertService;
    private readonly IPoetryService _poetryService;
    private readonly IPreferencesService _preferencesService;

    public TodayPoetryService(IAlertService alertService, IPoetryService poetryService,IPreferencesService preferencesService)
    {
        _alertService = alertService;
        _poetryService = poetryService;
        _preferencesService = preferencesService;
    }

    public async Task<TodayPoetry> GetTodayPoetryAsync()
    {
        //获取一个token
        var token = await GetTokenAsync();
        if (string.IsNullOrEmpty(token))
        {
            //没有获得token，那么从本地取数据
            return await GetLocalPoetryAsync();
        }
        //今日诗词API规定，访问请求时，需要在http头中带上token
        using var httpClient = new HttpClient();
        httpClient.DefaultRequestHeaders.Add("X-User-Token", token);
        HttpResponseMessage response;
        try
        {
            response = await httpClient.GetAsync("https://v2.jinrishici.com/sentence");
            response.EnsureSuccessStatusCode();
        }
        catch (Exception e)
        {
            _alertService.Alert("网络连接错误", $"详情：{e.Message}", "确定");
            return await GetLocalPoetryAsync();
        }
        var sentence = await response.Content.ReadFromJsonAsync<JinrishiciSentence>();
        var data = sentence.data;
        return new TodayPoetry
        {
            Snippet = data.content,
            Name = data.origin.title,
            Dynasty = data.origin.dynasty,
            Author = data.origin.author,
            Content = string.Join("\n", data.origin.content),
            Source = TodayPoetrySource.Jinrishici
        };
    }

    private async Task<string> GetTokenAsync()
    {
        //先从键值对中读取，如果有就返回，如果没有就去获取然后存下来
        var token = _preferencesService.Get(TokenKey, string.Empty);
        if (!string.IsNullOrEmpty(token)) return token;
        
        using var httpClient = new HttpClient();
        HttpResponseMessage response;
        try
        {
            response = await httpClient.GetAsync("https://v2.jinrishici.com/token");
            response.EnsureSuccessStatusCode();//判断是不是200，不是就直接进入异常
        }
        catch (Exception e)
        {
            //如果没联网，或者访问不是200，则会到这里来
            //todo：给用户提示一条信息，如果自己不想去做，就交给一个service去做
            _alertService.Alert("网络连接错误", $"详情：{e.Message}", "确定");
            return string.Empty;
        }
        var jinrishiciToken = await response.Content.ReadFromJsonAsync<JinrishiciToken>();
        token= jinrishiciToken.data;
        //将获取的token存储起来
        _preferencesService.Set(TokenKey,token);
        return token;
    }

    private async Task<TodayPoetry> GetLocalPoetryAsync()
    {
        var poetry = (await _poetryService.GetPoetryListAsync(
            Expression.Lambda<Func<Poetry, bool>>(Expression.Constant(true), Expression.Parameter(typeof(Poetry), "x")),
            new Random().Next(30), 1)).First();//跳过随机条，返回一条
        return new TodayPoetry
        {
            Snippet = poetry.Snippet,
            Name = poetry.Name,
            Dynasty = poetry.Dynasty,
            Author = poetry.Author,
            Content = poetry.Content,
            Source = TodayPoetrySource.Local
        };
    }
}

public class JinrishiciToken
{
    public string data { get; set; }
}

public class JinrishiciSentence
{
    public JinrishiciData? data { get; set; } = new();
    public string token { get; set; }
}

public class JinrishiciData
{
    public string? content { get; set; } = string.Empty;
    public JinrishiciOrigin? origin { get; set; } = new ();
}

public class JinrishiciOrigin
{
    public string? title { get; set; }=string.Empty;
    public string? dynasty { get; set; } = string.Empty;
    public string? author { get; set; } = string.Empty;
    public List<string>? content { get; set; } = new();
    public List<string>? translate { get; set; }=new ();
}
