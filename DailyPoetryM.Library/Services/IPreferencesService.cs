﻿namespace DailyPoetryM.Services;

public interface IPreferencesService
{
    int Get(string key,int defaultValue);
    void Set(string key,int value);

    string Get(string key, string defaultValue);
    void Set(string key, string value);

    DateTime Get(string key, DateTime defaultValue);
    void Set(string key, DateTime value);
}