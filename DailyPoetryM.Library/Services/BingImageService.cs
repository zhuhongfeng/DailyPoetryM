﻿namespace DailyPoetryM.Services;

public interface IBingImageService
{
    //两段式访问，优化用户体验
    //先调用GetTodayImageAsync会立刻返回缓存图片
    //后调用CheckUpdateAsync会访问web服务检查更新
    Task<TodayImage> GetTodayImageAsync();

    Task<BingImageServiceCheckUpdateResult> CheckUpdateAsync();
}
public class BingImageService : IBingImageService
{
    public const string BingUrl = "https://cn.bing.com";
    public const string ImageUrl = $"{BingUrl}/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=zh-CN";
    private readonly ITodayImageService _todayImageService;
    private readonly IAlertService _alertService;

    public BingImageService(ITodayImageService todayImageService, IAlertService alertService)
    {
        _todayImageService = todayImageService;
        _alertService = alertService;
    }

    public Task<TodayImage> GetTodayImageAsync()
    {
        return _todayImageService.GetTodayImageAsync(true);
    }

    public async Task<BingImageServiceCheckUpdateResult> CheckUpdateAsync()
    {
        //从api读取当前的数据，只取出信息，不要byte数组，防止耗时间
        var todayImage = await _todayImageService.GetTodayImageAsync(false);
        //如果还没过期就直接返回结果
        if (todayImage.ExpiresAt > DateTime.Now)
        {
            return new BingImageServiceCheckUpdateResult { HasUpdate = false };
        }
        //如果过期了就访问bing每日图片api
        using var httpClient = new HttpClient();
        HttpResponseMessage response;
        try
        {
            response=await httpClient.GetAsync(ImageUrl);
            response.EnsureSuccessStatusCode();
        }
        catch (Exception e)
        {
            _alertService.Alert("访问Bing每日图片报错", e.Message, "OK");
            return new BingImageServiceCheckUpdateResult { HasUpdate = false };
        }

        var bingImageList = await response.Content.ReadFromJsonAsync<BingImageOfTheDay>();
        var bingImage = bingImageList.Images.First();
        var bingImageFullStartDate = DateTime.ParseExact(bingImage.FullStartDate, "yyyyMMddHHmm", CultureInfo.InvariantCulture);
        var todayImageFullStartDate = DateTime.ParseExact(todayImage.FullStartDate, "yyyyMMddHHmm", CultureInfo.InvariantCulture);//如果这里报错，说明没有初始值
        //没更新时，推迟过期时间
        if (bingImageFullStartDate <= todayImageFullStartDate)
        {
            todayImage.ExpiresAt=DateTime.Now.AddHours(2);//推迟过期时间
            //只保存信息，不保存图片
            await _todayImageService.SaveTodayImageAsync(todayImage, true);
            return new BingImageServiceCheckUpdateResult { HasUpdate = false };
        }
        //有更新时，更新信息并下载图片
        todayImage = new TodayImage
        {
            FullStartDate = bingImage.FullStartDate,
            ExpiresAt = bingImageFullStartDate.AddDays(1),
            Copyright = bingImage.Copyright,
            CopyrightLink = bingImage.CopyrightLink
        };
        var bingImageUrl = $"{BingUrl}{bingImage.Url}";
        try
        {
            response =await httpClient.GetAsync(bingImageUrl);
            response.EnsureSuccessStatusCode();
        }
        catch (Exception e)
        {
            _alertService.Alert("访问Bing每日图片报错", e.Message, "OK");
            return new BingImageServiceCheckUpdateResult { HasUpdate = false };
        }
        //下载图片的二进制数据，byte数组
        todayImage.ImageBytes=await response.Content.ReadAsByteArrayAsync();
        //保存信息时还要保存图片
        await _todayImageService.SaveTodayImageAsync(todayImage, false);
        return new BingImageServiceCheckUpdateResult { HasUpdate = true, TodayImage =todayImage };
    }
}
//返回结果的包装类，一次调用，返回所有数据（所有的可能），防止多次调用，逻辑啰嗦
public class BingImageServiceCheckUpdateResult
{
    public bool HasUpdate { get; set; }//图片有没有更新
    public TodayImage TodayImage { get; set; } = new();//更新的内容，包含图片
}
public class BingImageOfTheDay
{
    public List<BingImageOfTheDayImage>? Images { get; set; }
}

public class BingImageOfTheDayImage
{
    public string? FullStartDate { get; set; }
    public string? Url { get; set; }
    public string? Copyright { get; set; }
    public string? CopyrightLink { get; set; }
}