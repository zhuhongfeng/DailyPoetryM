﻿namespace DailyPoetryM.Services;
//1、定义收藏变更事件参数，知道谁发生了变化。
public class FavoriteUpdatedEventArgs : EventArgs
{
    public FavoriteUpdatedEventArgs(Favorite updatedFavorite)
    {
        UpdatedFavorite=updatedFavorite;
    }
    public Favorite UpdatedFavorite { get; }//指明谁发生变化了
}
public interface IFavoriteService
{
    bool IsInitialized { get; }
    Task InitializeAsync();
    Task<Favorite?> GetFavoriteAsync(int poetryId);
    Task<IEnumerable<Favorite>> GetFavoritesAsync();
    Task SaveFavoriteAsync(Favorite favorite);
    //2、自定义事件。
    event EventHandler<FavoriteUpdatedEventArgs> Updated;
}
//用来存储这个类中使用的常量
public static class FavoriteServiceConst
{
    public const int Version = 1;//初始化后的版本号
    public const string VersionKey = $"{nameof(FavoriteServiceConst)}.{nameof(Version)}";
}
public class FavoriteService:IFavoriteService
{
    private readonly IPreferencesService _preferencesService;
    public FavoriteService(IPreferencesService preferencesService)
    {
        _preferencesService = preferencesService;
    }

    //读取本地存储的键值对，如果两个版本不一样，则需要执行初始化
    public bool IsInitialized => _preferencesService.Get(FavoriteServiceConst.VersionKey, 0)== FavoriteServiceConst.Version;

    public const string DbName = "favoritedb.sqlite3";
    public static readonly string FavoriteDbPath =
        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), DbName);
    //数据库连接
    private SQLiteAsyncConnection? _connection;
    private SQLiteAsyncConnection Connection => _connection ??= new SQLiteAsyncConnection(FavoriteDbPath);

    //初始化
    public async Task InitializeAsync()
    {
        await Connection.CreateTableAsync<Favorite>();//新创建的收藏数据库
        _preferencesService.Set(FavoriteServiceConst.VersionKey,FavoriteServiceConst.Version);
    }

    public async Task<Favorite?> GetFavoriteAsync(int poetryId) =>
       await Connection.Table<Favorite>().FirstOrDefaultAsync(x=>x.PoetyrId==poetryId);

    public async Task<IEnumerable<Favorite>> GetFavoritesAsync() =>
       await Connection.Table<Favorite>().Where(x => x.IsFavorite)
            .OrderByDescending(x => x.Timestamp).ToListAsync();

    public async Task SaveFavoriteAsync(Favorite favorite)
    {
        favorite.Timestamp = DateTime.Now.Ticks;
        await Connection.InsertOrReplaceAsync(favorite);
        //3、触发更新事件(在保存收藏数据之后)。
        Updated?.Invoke(this,new FavoriteUpdatedEventArgs(favorite));
    }

    public event EventHandler<FavoriteUpdatedEventArgs>? Updated;
}