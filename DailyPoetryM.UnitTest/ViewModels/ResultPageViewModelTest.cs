﻿namespace DailyPoetryM.UnitTest.ViewModels;

public class ResultPageViewModelTest:IDisposable
{
    public ResultPageViewModelTest()
    {
        File.Delete(PoetryService.PoetryDbPath);
    }
    public void Dispose()
    {
        File.Delete(PoetryService.PoetryDbPath);
    }
    [Fact]
    public async Task TestPoetriesDefault()
    {
        //准备查询条件，相当于Poetries.Where(x=>true)
        //Func<Poetry, bool>表示一个参数是Poetry返回值是bool的函数
        //因此可以让用户在程序运行时，任意组合多个查询条件，如：x.Id>5 && x.Author.Contains("李") && ...
        var where = Expression.Lambda<Func<Poetry, bool>>(Expression.Constant(true), Expression.Parameter(typeof(Poetry), "x"));
        var poetryService = await PoetryServiceTest.GetInitializedPoetryService();
        var resultPageViewModel = new ResultPageViewModel(poetryService,null);
        resultPageViewModel.Where=where;
        //测试变化的属性
        var statusList=new List<string>();
        //resultPageViewModel.PropertyChanged += (sender, args) =>
        //{
        //    if (args.PropertyName == nameof(ResultPageViewModel.Status))
        //    {
        //        //每次Status属性变化时，将它的值记录下来
        //        statusList.Add(resultPageViewModel.Status);
        //    }
        //};
        //测试时不要使用RelayCommand，而是直接使用它指向的方法(这样带来的坏处是公开了NavigatedTo方法)
        //await resultPageViewModel.NavigatedTo();
        //最好的做法是将NavigatedToCommand修改成AsyncRelayCommand
        resultPageViewModel.NavigatedToCommand.Execute(null);
        //然后在它后面加上如下语句，ExecutionTask，表示Task执行完毕了
        await resultPageViewModel.NavigatedToCommand.ExecutionTask!;
        //应该有PageSize,20条结果出现
        Assert.Equal(ResultPageViewModel.PageSize,resultPageViewModel.Poetries.Count);
        //Status应该按照以下流程变化Loading,Empty(还有数据)
        Assert.Equal(2,statusList.Count);
        //Assert.Equal(ResultPageViewModel.Loading, statusList[0]);
        Assert.Equal(string.Empty, statusList[1]);
        /*//因为数据库只有30条数据，如果再取一次，应该只有10条数据，Stauts是NoMoreResult
        await resultPageViewModel.Poetries.LoadMoreAsync();
        Assert.Equal(ResultPageViewModel.PageSize+10, resultPageViewModel.Poetries.Count);
        Assert.Equal(2+3, statusList.Count);
        Assert.Equal(ResultPageViewModel.Loading, statusList[2]);
        Assert.Equal(string.Empty, statusList[3]);
        Assert.Equal(ResultPageViewModel.NoMoreResult, statusList[4]);*/
        await poetryService.CloseAsync();
    }
}