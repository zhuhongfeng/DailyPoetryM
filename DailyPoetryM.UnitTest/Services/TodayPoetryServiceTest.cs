﻿namespace DailyPoetryM.UnitTest.Services;

public class TodayPoetryServiceTest
{
    [Fact]
    public async Task TestGetTodayPoetryAsyncDefault()
    {
        var tps = new TodayPoetryService(null,null,null);
        var result=await tps.GetTodayPoetryAsync();
        //验证返回结果是不是空的
        Assert.NotNull(result);
        //测试今日诗词是不是从API获取的
        Assert.True(result.Source==TodayPoetrySource.Jinrishici);
    }
}