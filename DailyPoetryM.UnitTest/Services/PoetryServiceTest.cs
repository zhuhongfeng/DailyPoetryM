﻿namespace DailyPoetryM.UnitTest.Services;

public class PoetryServiceTest:IDisposable
{
    public PoetryServiceTest()
    {
        //每次运行单元测试前，需要执行的操作，放在构造器中，new对象时执行
        File.Delete(PoetryService.PoetryDbPath);
    }
    public void Dispose()
    {
        //每次运行单元测试后，需要执行的操作，放在Dispose中，对象释放时执行
        File.Delete(PoetryService.PoetryDbPath);
    }

    [Fact]//测试初始化
    public void TestIsInitializedDefault()
    {
        //需要安装Moq包，新建一个IPreferencesService的虚假实现类工具，Object才是虚假实例对象
        var preferencesServiceMock = new Mock<IPreferencesService>();
        //设置这个虚假实现类的行为，Setup行为，Returns返回值
        //当调用Get函数时，返回Version
        preferencesServiceMock.
            Setup(x => x.Get(PoetryServiceConst.VersionKey, 0))
            .Returns(PoetryServiceConst.Version);

        var poetryService = new PoetryService(preferencesServiceMock.Object);
        Assert.True(poetryService.IsInitialized);//断言IsInitialized的值是true
        //Assert.False(poetryService.IsInitialized);//断言IsInitialized的值是false
    }

    [Fact]//测试InitializeAsync函数，使用嵌入式资源拷贝文件
    public async Task TestInitializeAsyncDefault()
    {
        var preferencesServiceMock = new Mock<IPreferencesService>();
        var poetryService = new PoetryService(preferencesServiceMock.Object);
        //先检验文件不存在
        Assert.False(File.Exists(PoetryService.PoetryDbPath));
        //拷贝文件
        await poetryService.InitializeAsync();
        //检验文件存在
        Assert.True(File.Exists(PoetryService.PoetryDbPath));
        //验证Set函数是否被调用过一次
        preferencesServiceMock.Verify(x=>x.Set(PoetryServiceConst.VersionKey,PoetryServiceConst.Version),Times.Once);
    }

    public static async Task<PoetryService> GetInitializedPoetryService()
    {
        //初始化数据库
        var preferencesServiceMock = new Mock<IPreferencesService>();
        var poetryService = new PoetryService(preferencesServiceMock.Object);
        await poetryService.InitializeAsync();
        return poetryService;   
    }

    [Fact]
    public async Task TestGetPoetryAsyncDefault()
    {
        var poetryService =await GetInitializedPoetryService();
        var poetry = await poetryService.GetPoetryAsync(10001);
        //验证数据是否正确Assert.Equal(期望值,被验证值)，使用DB Browser for SQLite打开数据库查看值
        Assert.Equal("临江仙 · 夜归临皋", poetry.Name);
        //关闭数据库连接
        await poetryService.CloseAsync();
    }

    [Fact]
    public async Task TestGetPoetryListAsyncDefault()
    {
        var poetryService = await GetInitializedPoetryService();
        //取出数据库中所有的数据
        var poetryList = await poetryService.GetPoetryListAsync(
            Expression.Lambda < Func < Poetry,bool >> (Expression.Constant(true),
                Expression.Parameter(typeof(Poetry), "x")), 0, int.MaxValue);
        /* Expression.Lambda < Func < Poetry,bool >> (Expression.Constant(true),
                        Expression.Parameter(typeof(Poetry), "x"))
         * 相当于Connection.Table<Poetry>.Where(x=>true);
         * 相当于select * from Poetry where true
         */
        //因为数据库中有30条数据，因此验证总数即可。
        Assert.Equal(30,poetryList.Count());
        await poetryService.CloseAsync();
    }
}