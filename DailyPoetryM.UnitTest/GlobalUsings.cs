global using Xunit;
global using DailyPoetryM.Services;
global using System.Linq.Expressions;
global using DailyPoetryM.Models;
global using Moq;
global using DailyPoetryM.UnitTest.Services;
global using DailyPoetryM.ViewModels;