﻿namespace DailyPoetryM.Views;

public partial class MainPage : ContentPage
{
    public MainPage(ResultPageViewModel viewModel)
    {
        InitializeComponent();
        //绑定ViewModel
        BindingContext =viewModel;
    }
}