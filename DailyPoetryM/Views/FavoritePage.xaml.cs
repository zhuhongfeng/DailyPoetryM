namespace DailyPoetryM.Views;

public partial class FavoritePage : ContentPage
{
	public FavoritePage(FavoritePageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext=viewModel;
	}
}