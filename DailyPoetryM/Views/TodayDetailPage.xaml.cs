namespace DailyPoetryM.Views;

public partial class TodayDetailPage : ContentPage
{
	public TodayDetailPage(TodayDetailPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext = viewModel;
	}
}