namespace DailyPoetryM.Views;

public partial class QueryPage : ContentPage
{
	public QueryPage(QueryPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext=viewModel;
	}
}