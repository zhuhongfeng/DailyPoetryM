namespace DailyPoetryM.Views;

public partial class TodayPage : ContentPage
{
	public TodayPage(TodayPageViewModel viewModel)
	{
		InitializeComponent();
		BindingContext=viewModel;
	}
}