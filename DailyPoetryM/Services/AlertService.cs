﻿namespace DailyPoetryM.Services;
public class AlertService : IAlertService
{
    public void Alert(string title, string message, string accept)
    {
        //在主线程上显示警告
        MainThread.BeginInvokeOnMainThread(async () =>
        {
            await Shell.Current.DisplayAlert(title, message, accept);
        });
    }
}