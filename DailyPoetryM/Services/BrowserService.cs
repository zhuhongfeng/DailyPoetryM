﻿namespace DailyPoetryM.Services;

public class BrowserService:IBrowserService
{
    public Task OpenAsync(string url)=>Browser.OpenAsync(url);
}