﻿namespace DailyPoetryM.Services;

public class NavigationService:INavigationService
{
    public Task NavigateToAsync(string route, IDictionary<string, object> routeParameters = null)
    {
        return routeParameters is null
            ? Shell.Current.GoToAsync(route)
            : Shell.Current.GoToAsync(route, routeParameters);
        
    }
}