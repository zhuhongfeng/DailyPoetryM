﻿namespace DailyPoetryM.Services;
//实现Library项目中未实现的接口,使用MAUI的键值存储机制Preferences
public class PreferencesService:IPreferencesService
{
    public int Get(string key, int defaultValue) => Preferences.Get(key, defaultValue);
    public void Set(string key, int value) => Preferences.Set(key, value);

    public string Get(string key, string defaultValue) => Preferences.Get(key, defaultValue);
    public void Set(string key, string value) => Preferences.Set(key, value);

    public DateTime Get(string key, DateTime defaultValue) => Preferences.Get(key, defaultValue);
    public void Set(string key, DateTime value) => Preferences.Set(key, value);
}