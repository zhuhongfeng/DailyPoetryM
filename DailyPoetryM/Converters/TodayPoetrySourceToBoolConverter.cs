﻿namespace DailyPoetryM.Converters;

public class TodayPoetrySourceToBoolConverter:IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        //判断被转换值value是不是字符串，parameter期望值是不是字符串，被转换值是不是期望值
        return value is string source 
               && parameter is string expectedSource 
               && source == expectedSource;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}