﻿global using Microsoft.Extensions.Logging;
global using CommunityToolkit.Maui;
global using System.Globalization;


global using DailyPoetryM.Models;
global using DailyPoetryM.Views;
global using DailyPoetryM.ViewModels;
global using DailyPoetryM.Services;
