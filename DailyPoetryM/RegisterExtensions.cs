﻿namespace DailyPoetryM;
//各种注册的扩展类
public static class RegisterExtensions
{
    /// <summary>
    /// 注册依赖注入服务
    /// </summary>
    public static MauiAppBuilder RegisterAppServices(this MauiAppBuilder builder)
    {
        builder.Services.AddScoped<IPreferencesService, PreferencesService>();
        builder.Services.AddScoped<IAlertService, AlertService>();
        builder.Services.AddScoped<INavigationService, NavigationService>();

        builder.Services.AddScoped<IPoetryService, PoetryService>();
        builder.Services.AddScoped<ITodayPoetryService, TodayPoetryService>();

        builder.Services.AddScoped<IBrowserService, BrowserService>();
        builder.Services.AddScoped<ITodayImageService, TodayImageService>();
        builder.Services.AddScoped<IBingImageService, BingImageService>();
        builder.Services.AddScoped<IFavoriteService, FavoriteService>();

        return builder;
    }

    /// <summary>
    /// 注册View和ViewModel
    /// </summary>
    public static MauiAppBuilder RegisterViewModels(this MauiAppBuilder builder)
    {
        //注册依赖注入,同时注册view和viewmodel，由.UseMauiCommunityToolkit()提供
        builder.Services.AddScoped<MainPage, ResultPageViewModel>();
        builder.Services.AddTransient<DetailPage,DetailPageViewModel>();
        builder.Services.AddScoped<TodayPage, TodayPageViewModel>();
        builder.Services.AddTransient<TodayDetailPage, TodayDetailPageViewModel>();
        builder.Services.AddScoped<QueryPage, QueryPageViewModel>();
        builder.Services.AddScoped<FavoritePage, FavoritePageViewModel>();
        return builder;
    }

    /// <summary>
    /// 注册未在Shell层次结构中显示注册路由的页面(全局导航)
    /// </summary>
    public static void RegisterRouting()
    {
        Routing.RegisterRoute(nameof(DetailPage),typeof(DetailPage));
        Routing.RegisterRoute(nameof(TodayDetailPage),typeof(TodayDetailPage));
    }
}